﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using MyTestProject.Controllers;
using NUnit.Framework;
using Moq;
using MyTestProject.Provider;

namespace MyTestProject.Tests.Controllers
{
    [TestFixture]
    class FileSystemControllerTest
    {
        [Test]
        public void ShouldReturnBadRequest_WhenPathIsNotExist()
        {
            var mock = new Mock<IFileSystemProvider>();
            mock.Setup(x => x.ItemExists("c:\\wind"))
                .Returns(false);

            var fakeProvider = mock.Object;

            var r = new FilesController(fakeProvider) { Request = new System.Net.Http.HttpRequestMessage() };
            var response = r.Get("c:\\wind");
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }


    }

}
