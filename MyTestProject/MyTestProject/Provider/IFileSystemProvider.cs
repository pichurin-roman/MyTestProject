﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTestProject.Provider
{
   public interface IFileSystemProvider
    {
        bool ItemExists(string path);
    }
}
